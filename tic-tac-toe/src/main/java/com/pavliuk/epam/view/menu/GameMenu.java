package com.pavliuk.epam.view.menu;

import com.pavliuk.epam.TicTacToeApplication;
import com.pavliuk.epam.controller.GameService;
import com.pavliuk.epam.controller.RandomSymbolService;
import com.pavliuk.epam.model.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class GameMenu {
  private static Logger logger = LogManager.getLogger(TicTacToeApplication.class);
  private GameService gameService;
  private RandomSymbolService randomSymbolService;
  private Scanner scanner;
  private Player player2;
  private Player player1;

  public GameMenu() {
    this.gameService = new GameService();
    this.scanner = new Scanner(System.in);
    this.randomSymbolService = new RandomSymbolService();
    this.player1 = new Player();
    this.player2 = new Player();
  }

private boolean finish = true;
  public void run() {
    logger.info("Hello! This is Tic Tac Toe, the best game in the world! \nPress \"Y\" to start game " +
            "\nPress any other key to exit");
    while (finish) {
      if (scanner.next().equals("Y")) {
        while (finish) {
         logger.info("Enter player 1 name");
          player1.setName(scanner.next());
          player1.setSymbol(randomSymbolService.getSymbol());
          printPlayerInfo(player1);
          logger.info("Enter player 2 name");
          player2.setName(scanner.next());
          while (true) {
            String symbol = randomSymbolService.getSymbol();
            if (!player1.getSymbol().equals(symbol)) {
              player2.setSymbol(symbol);
              break;
            }
          }
          printPlayerInfo(player2);
          logger.info("Lets start the game");
          try {
            boolean finished = false;

            while (!finished) {
              logger.info(player1.getName()
                      + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
              gameService.printGameField();
              makeTurn(player1.getSymbol());
              finished = isFinished(gameService);
              if (finished) {
                break;
              }
              logger.info(player2.getName()
                      + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
              gameService.printGameField();
              makeTurn(player2.getSymbol());
              finished = isFinished(gameService);
            }
          } catch (Exception e) {
              logger.error("Invalid format entered ", e);
          }
          logger.info("\nPress \"Y\" to restart \nPress any other key to exit");
          if (scanner.next().equals("Y")) {
            gameService.fillGameField();
          }
          else { finish = false;
          break;
          }
        }
      } else {
        break;
      }
      logger.info("Bye! Have a beautiful day!");
    }
  }

  private boolean isFinished(GameService gameService) {
    if (gameService.win(player1.getSymbol())) {
      gameService.printGameField();
      logger.info(player1.getName() + " you won!");
      return true;
    } else if (gameService.win(player2.getSymbol())) {
      gameService.printGameField();
      logger.info(player2.getName() + " you won!");
      return true;
    } else if (gameService.isDraw(player1.getSymbol(), player2.getSymbol())) {
      gameService.printGameField();
      logger.info("Draw!");
      return true;
    }
    return false;
  }
  
  private void makeTurn(String symbol) {
    while (true) {
      String choice = scanner.next();
      String[] split = choice.split("\\+");
      int horizontalCoordinate = Integer.parseInt(split[0]);
      int verticalCoordinate = Integer.parseInt(split[1]);
      if (!gameService.isCoordinateEmpty(horizontalCoordinate, verticalCoordinate)) {
        logger.info("There is already a symbol present in this cell, please try again!");
      } else {
        gameService.putSymbol(symbol, horizontalCoordinate, verticalCoordinate);
        break;
      }
    }
  }

  private void printPlayerInfo(Player player) {
    logger.info("Player name: " + player.getName() + " and symbol: " + player.getSymbol());
  }

}
