package com.pavliuk.epam;

import com.pavliuk.epam.view.menu.GameMenu;

public class TicTacToeApplication {

  public static void main(String[] args) {
    GameMenu gameMenu = new GameMenu();
    gameMenu.run();
  }

}
