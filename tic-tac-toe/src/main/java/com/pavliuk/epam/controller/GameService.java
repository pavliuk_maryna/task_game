package com.pavliuk.epam.controller;

public class GameService {

  private String[][] gameField = new String[3][3];

  public GameService() {
    fillGameField();
  }

  public void printGameField() {
    // TODO refactor
    System.out.print("   ");
    for (int i = 0; i < gameField.length; i++) {
      System.out.print(" " + i + " ");
    }
    System.out.println();
    for (int i = 0; i < gameField.length; i++) {
      System.out.print(" " + (i) + " ");
      for (int j = 0; j < gameField[0].length; j++) {
        System.out.print(gameField[i][j]);
      }
      System.out.println();
    }
  }

  public boolean isCoordinateEmpty(int horizontalCoordinate, int verticalCoordinate) {
   return gameField[horizontalCoordinate][verticalCoordinate].contains("-");
  }

  public void putSymbol(String symbol, int horizontalCoordinate, int verticalCoordinate) {
    gameField[horizontalCoordinate][verticalCoordinate] = gameField[horizontalCoordinate][verticalCoordinate]
        .replace("-", symbol);
  }

  public boolean win(String symbol) {
    // horizontal check
    for (int i = 0; i < gameField.length; i++) {
      if (gameField[i][0].contains(symbol) && gameField[i][1].contains(symbol) && gameField[i][2]
          .contains(symbol)) {
        return true;
      } else if
        //vertical check
      (gameField[0][i].contains(symbol) && gameField[1][i].contains(symbol) && gameField[2][i]
              .contains(symbol)) {
        return true;
      }
    }
    // cross check
    if (gameField[1][1].contains(symbol)) {
      return (gameField[0][0].contains(symbol) && gameField[2][2].contains(symbol)) || ((
          gameField[0][2].contains(symbol) && gameField[2][0].contains(symbol)));
    }
    return false;
  }

  public boolean isDraw(String symbol1, String symbol2) {
    for (int i = 0; i < gameField.length; i++) {
      for (int j = 0; j < gameField[0].length; j++) {
        if (!gameField[i][j].contains(symbol1) && !gameField[i][j].contains(symbol2)) {
          return false;
        }
      }
    }
    return true;
  }

  public void fillGameField() {
    for (int i = 0; i < gameField.length; i++) {
      for (int j = 0; j < gameField[0].length; j++) {
        gameField[i][j] = " - ";
      }
    }
  }
}
